@extends('layouts.app')

@section('content')
<div class="card__container">
    <article class="card__box">
        <div class="card__preview"></div>
        <div class="card__wrapper">
            <h2 class="card__title">
                <a href="/kek" name="Closures in JavaScript">
                    Closures in JavaScript
                </a>
            </h2>
            <p class="card__text">
                A closure is the combination of a function bundled together (enclosed) with references to ...
            </p>
            <div class="card__footer">
                <time class="card__date">2019/09/11</time>
                <div class="card__views">
                    2 v
                </div>
            </div>
        </div>
    </article>
</div>
@endsection
