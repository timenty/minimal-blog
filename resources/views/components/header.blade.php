<header class="ctr">
    <nav>
        <a class="heading__label" href="{{ url('/') }}">Timenty's</a>
        <div class="heading-other__container">
            <a class="heading-other__element" href="#">О нас</a>
            <a class="heading-other__element" href="#">Контакты</a>
        </div>
    </nav>
</header>
